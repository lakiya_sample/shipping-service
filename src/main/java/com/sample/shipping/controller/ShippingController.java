package com.sample.shipping.controller;

import com.sample.shipping.model.ShippingInfo;
import com.sample.shipping.repository.ShippingRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("shipping")
public class ShippingController {
    private final ShippingRepository shippingRepository;

    public ShippingController(ShippingRepository shippingRepository) {
        this.shippingRepository = shippingRepository;
    }

    @GetMapping("{orderId}")
    public ResponseEntity<ShippingInfo> getOrderShippingInfo(@PathVariable Long orderId){
        Optional<ShippingInfo> optShippingInfo = this.shippingRepository.findByOrderId(orderId);
        if(optShippingInfo.isPresent()){
            return new ResponseEntity<>(optShippingInfo.get(), HttpStatus.OK);
        }
        else{
            throw new RuntimeException("Order not found with give id");
        }
    }

    @PostMapping
    public ShippingInfo saveShippingInfo(@RequestBody ShippingInfo shippingInfo){
       return this.shippingRepository.save(shippingInfo);
    }

    @PutMapping
    public ShippingInfo updateShippingInfo(@RequestBody ShippingInfo shippingInfo){
        return this.shippingRepository.save(shippingInfo);
    }

    @DeleteMapping("{orderId}")
    public void deleteShippingInfo(@PathVariable Long orderId){
        Optional<ShippingInfo> optShippingInfo = this.shippingRepository.findByOrderId(orderId);
        if(optShippingInfo.isPresent()){
            this.shippingRepository.delete(optShippingInfo.get());
        }
        else{
            throw new RuntimeException("Order not found with give id");
        }
    }
}